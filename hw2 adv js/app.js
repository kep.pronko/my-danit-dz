
  class List {
      constructor(books){
          this.books = books;
          this.list = this._render();
      }
      _render() {
        const list = document.createElement("ul");
        this.books.forEach(item => {
          try{
            if(Object.keys(item).length === 2 && item.price && item.name){
              throw new Error("not author");
            }
            else{
                if(Object.keys(item).length === 2 && item.author && item.name){
                  throw new Error("not price");
                }else{
                  if (Object.keys(item).length === 3) {
                    const items = document.createElement("li");
                    items.textContent = item.author + ' '+ item.name + ' '+ item.price ;
                    list.appendChild(items);
                    }
                }
            }
          }
          catch(e){console.log(e.message);}
        
      });
      
        return list;
     }
  }
  const listnew = new List(
    [
        { 
          author: "Скотт Бэккер",
          name: "Тьма, что приходит прежде",
          price: 70 
        }, 
        {
         author: "Скотт Бэккер",
         name: "Воин-пророк",
        }, 
        { 
          name: "Тысячекратная мысль",
          price: 70
        }, 
        { 
          author: "Скотт Бэккер",
          name: "Нечестивый Консульт",
          price: 70
        }, 
        {
         author: "Дарья Донцова",
         name: "Детектив на диете",
         price: 40
        },
        {
         author: "Дарья Донцова",
         name: "Дед Снегур и Морозочка",
        }
      ]
 );
  const root = document.getElementById("root");
root.append(listnew.list);