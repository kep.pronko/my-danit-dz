const express = require('express');
// import express from 'express'

const app = express();

const port = 8085;

const card = [
    {"id": 1,"name": "Красивое платье","price": 1200,"url": "https://i.wdb.im/products/SN-128-Rozmari-3.2000x3000w.jpg","articul": 1,"color": "#fff"},
    {"id": 2,
    "name": "Красивое платье",
    "price": 1400,
    "url": "http://bs-moda.com/image/magictoolbox_cache/70f8cbb875f8b8cc4899f4b7650f2b47/6/4/644_product/original/830226258/ameli-20-03-vanilla-sky-01.jpg",
    "articul": 1,
    "color": "#fff"},
    {"id": 3,
    "name": "Красивое платье",
    "price": 1600,
    "url": "https://wed-shop.com.ua/image/data/procat%202020/2020%20Lanesta%20/SEraphime%201.jpg",
    "articul": 1,
    "color": "#fff"},
    {"id": 4,
    "name": "Красивое платье",
    "price": 1600,
    "url": "https://cdn.marytrufel.ru/HrSX-kac4wEnTti4IZFNelyDgMznnNoIq4b4sBhtuaY/rs:fill:1121:1682/q:95/plain/http://10.7.86.127/uploads/dress/3745/photo/69562/_______3_.jpg",
    "articul": 1,
    "color": "#fff"},
    {"id": 5,
    "name": "Красивое платье",
    "price": 1600,
    "url": "https://millennium-mod.com.ua/wp-content/uploads/F4C12818-0E23-45DD-A8F0-4A4CDF0376DE.jpeg",
    "articul": 1,
    "color": "#fff"},
    {"id": 6,
    "name": "Красивое платье",
    "price": 1600,
    "url": "https://venera-salon.com.ua/wp-content/uploads/2021/02/4m.jpg",
    "articul": 1,
    "color": "#fff"},
    {"id": 7,
    "name": "Красивое платье",
    "price": 1600,
    "url": "https://www.madeira-wedding.com.ua/files/products/Bella_(1).1800x1800w.jpg?71d090c1eff10c39ed6d9bf064b420a7",
    "articul": 1,
    "color": "#fff"},
    {"id": 8,
    "name": "Красивое платье",
    "price": 1600,
    "url": "https://vero.in.ua/wp-content/uploads/%D0%BF%D1%80%D1%8F%D0%BC%D0%BE%D0%B5-%D1%81%D0%B2%D0%B0%D0%B4%D0%B5%D0%B1%D0%BD%D0%BE%D0%B5-%D0%BF%D0%BB%D0%B0%D1%82%D1%8C%D0%B5-%D1%81-%D1%80%D1%83%D0%BA%D0%B0%D0%B2%D0%B0%D0%BC%D0%B8.jpg",
    "articul": 1,
    "color": "#fff"},
    {"id": 9,
    "name": "Красивое платье",
    "price": 1600,
    "url": "https://venera-salon.com.ua/wp-content/uploads/2021/02/0-02-04-054610ca2d16e5dcacd49eedc1ae93abb114ab717802977af44891496e239f01_71c65913.jpg",
    "articul": 1,
    "color": "#fff"},
    {"id": 10,
    "name": "Красивое платье",
    "price": 1600,
    "url": "https://lamour-salon.ru/slir/w570-h499-c1:1/userfiles/ANTARIA.jpg",
    "articul": 1,
    "color": "#fff"},
    {"id": 11,
    "name": "Красивое платье",
    "price": 1600,
    "url": "https://www.wedding-magazine.ru/images/articles/55978/gallery/avp4105av_ok1def-1.jpg",
    "articul": 1,
    "color": "#fff"},
    {"id": 12,
    "name": "Красивое платье",
    "price": 1600,
    "url": "https://www.anabel.ua/wp-content/uploads/2020/10/IMG_1418-2.jpg",
    "articul": 1,
    "color": "#fff"}
]

app.get('/api/card', (req, res) => {
  res.send(card)
})

app.get('/api/card/:cardId', (req, res) => {
  const cardId = +req.params.cardId
  const card = card.find(e => e.id === cardId)
  res.send(card)
})

app.listen(port, () => {
  console.log(`Server started on port ${port}`)
})
