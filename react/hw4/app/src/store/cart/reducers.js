import types from './types'

const initialState = {
  show: false,
  modal: 
    {
        id: 1,
        header: "Реально добавить это платье в корзину?",
        closeButton: true,
        text: "Бомба",
    }
,
  cart: [],
  chouseId: null
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_SHOW_MODAL: {
      return {...state, show: action.data}
    }
    case types.SET_MODAL_DATA: {
      return {...state, modal: action.data}
    }
    case types.SET_CART_DATA: {
      return {...state, cart: action.data}
    }
    case types.SET_CHOUSE_ID: {
      return {...state, chouseId: action.data}
    }
    default:  return state

  }
}


export default reducer;
