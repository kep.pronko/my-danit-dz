import axios from "axios";
import actions from './actions'



export default {
  setShowModal: actions.setShowModal,
  setCartData: actions.setCartData,
  setModalData: actions.setModalData,
  setChouseId: actions.setChouseId,
}
