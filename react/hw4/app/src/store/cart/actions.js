import types from './types'

const setShowModal = (cartData) => ({type: types.SET_SHOW_MODAL, data: cartData})
const setModalData = (cartData) => ({type: types.SET_MODAL_DATA, data: cartData})
const setCartData = (cartData) => ({type: types.SET_CART_DATA, data: cartData})
const setChouseId = (cartData) => ({type: types.SET_CHOUSE_ID, data: cartData})
export default {
  setShowModal,
  setCartData,
  setModalData,
  setChouseId
}