import types from './types'

const initialState = {
    data: null
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_CARDS_DATA: {
      return {...state, data: action.data}
    }
    default:  return state

  }
}


export default reducer;
