import {combineReducers} from "redux";
import cards from './cards'
import cart from './cart'
import favorite from './favorite'


const reducer = combineReducers({
    cards,
    cart,
    favorite
})


export default reducer;
