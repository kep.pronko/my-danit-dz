import types from './types'

const initialState = {
  favorite: [],
  filled: []
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_FAVORITES_DATA: {
      return {...state, favorite: action.data}
    }
    case types.SET_FILLED: {
      return {...state, filled: action.data}
    }
    default:  return state

  }
}


export default reducer;
