const SET_FAVORITES_DATA = 'app/favorite/SET_FAVORITES_DATA';
const SET_FILLED = 'app/favorite/SET_FILLED';

export default {
  SET_FAVORITES_DATA,
  SET_FILLED,
}