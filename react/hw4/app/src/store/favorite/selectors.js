
const getFavorite = () => state => state.favorite.favorite;
const getFilled = () => state => state.favorite.filled;
export default {
  getFavorite,
  getFilled
}