import types from './types'

const setFavorites = (favoritesData) => ({type: types.SET_FAVORITES_DATA, data: favoritesData})
const setFilled = (favoritesData) => ({type: types.SET_FILLED, data: favoritesData})
export default {
  setFavorites,
  setFilled
}