import React from 'react';
import {Route, Switch, Redirect} from "react-router-dom";
import Favorites from "../pages/Favorites/Favorites";
import Cart from '../pages/Cart/Cart';
import Main from '../pages/Main/Main';
import Page404 from "../pages/Page404/Page404";

const AppRoutes = () => {
  return (
    <Switch>
      <Redirect exact from='/' to='/main'/>
      <Route exact path='/main'><Main /></Route>
      <Route exact path='/favorites'><Favorites /></Route>
      <Route exact path='/cart'><Cart/></Route>
      <Route path='*'><Page404/></Route>
    </Switch>
  )
};

export default AppRoutes;