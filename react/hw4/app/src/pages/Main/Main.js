import React, {  useEffect } from "react";
import {Modal} from "../../components/Modal/Modal";
import Cards from "../../components/Cards/Cards";
import { useDispatch,useSelector } from "react-redux";
import {cardsSelectors} from '../../store/cards';
import {cartOperations} from '../../store/cart';
import {cartSelectors} from '../../store/cart';
import {favoriteOperations} from '../../store/favorite';
import {favoriteSelectors} from '../../store/favorite';



const Main = () => {
    const show = useSelector(cartSelectors.getShow());
    const cards = useSelector(cardsSelectors.getCards())
    const favorites = useSelector(favoriteSelectors.getFavorite());
    const cart = useSelector(cartSelectors.getCart());
    const chouseId = useSelector(cartSelectors.getChouseId());
    const dispatch = useDispatch();

    useEffect(() => {
        const localStorageFavorites = JSON.parse(localStorage.getItem('favorites'));

        dispatch(favoriteOperations.setFavorites(localStorageFavorites));
        dispatch(favoriteOperations.setFilled(handleFilled(localStorageFavorites)));

        const localStorageCart = JSON.parse(localStorage.getItem('cart'));
        dispatch(cartOperations.setCartData(localStorageCart));
    }, [dispatch]);

    const handleModalClose = () => {
        dispatch(cartOperations.setShowModal(false));
        dispatch(cartOperations.setChouseId(null));
    };

    const handleModalOpen = (event) => {
        const index = Number(event.target.id);
        dispatch(cartOperations.setShowModal(true));
        dispatch(cartOperations.setChouseId(index));
    };

    const addToCart = () => {
        const newCart = cart ? [...cart] : [];
        const card = cards?.find(card => card.id === chouseId);
        newCart.push(card);
        console.log(newCart);
        dispatch(cartOperations.setCartData(newCart));
        dispatch(cartOperations.setShowModal(false));
        window.localStorage.setItem('cart', JSON.stringify(newCart));
    };

    const updateFavorite = (favoriteItem) => {
        let newFavorites = favorites ? [...favorites] : [];
        const favorite = [];
        favorite.push(favoriteItem)

        const itemAlreadyExist = favorites?.find(item => item.id === favoriteItem.id);

        if (itemAlreadyExist) {
            const index = newFavorites?.findIndex(item => item.id === favoriteItem.id);
            newFavorites.splice(index, 1);
        } else {
            newFavorites = [...newFavorites, ...favorite];
        }

        dispatch(favoriteOperations.setFilled(handleFilled(newFavorites)));

        dispatch(favoriteOperations.setFavorites(newFavorites));
        localStorage.setItem('favorites', JSON.stringify(newFavorites));
    };

    const handleFilled = (favoritesList) => favoritesList?.map(item => item.id);

    return (
        <React.Fragment>
            { show && (
                <Modal
                    handleModalClose={handleModalClose}
                    addToCart={addToCart}
                />
            )}
            <Cards cards={cards} onClick={handleModalOpen} updateFavorite={updateFavorite} />
        </React.Fragment>
    )
}

export default Main

