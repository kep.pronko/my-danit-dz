import React, { useEffect } from "react";
import {Modal} from "../../components/Modal/Modal";
import Cards from "../../components/Cards/Cards";
import { useDispatch,useSelector } from "react-redux";
import {cartOperations} from '../../store/cart';
import {cartSelectors} from '../../store/cart';
import {favoriteOperations} from '../../store/favorite';
import {favoriteSelectors} from '../../store/favorite';

const Favorites = () => {
    const show = useSelector(cartSelectors.getShow());
    const favorites = useSelector(favoriteSelectors.getFavorite());
    const cart = useSelector(cartSelectors.getCart());
    const chouseId = useSelector(cartSelectors.getChouseId());
    const filled = useSelector(favoriteSelectors.getFilled());
    const dispatch = useDispatch();

    useEffect(() => {
        const localStorageFavorites = JSON.parse(localStorage.getItem('favorites'));
        dispatch(favoriteOperations.setFavorites(localStorageFavorites));
        dispatch(favoriteOperations.setFilled(handleFilled(localStorageFavorites)));
    }, [dispatch]);

    const handleModalClose = () => {
        dispatch(cartOperations.setShowModal(false));
        dispatch(cartOperations.setChouseId(null));
    };

    const handleModalOpen = (event) => {
        const index = Number(event.target.id);
        dispatch(cartOperations.setShowModal(true));
        dispatch(cartOperations.setChouseId(index));
    };

    const addToCart = () => {
        const index = cart.concat(chouseId)
        dispatch(cartOperations.setCartData(index));
        dispatch(cartOperations.setShowModal(false));
        window.localStorage.setItem('cart', index);
    };

    const deleteFavorite = (favoriteItem) => {
        let newFavorites = [...favorites];
        const favorite = [];
        favorite.push(favoriteItem)

        const index = newFavorites.findIndex(item => item.id === favoriteItem.id);
        if (index !== -1) {
            newFavorites.splice(index, 1);
            dispatch(favoriteOperations.setFavorites(newFavorites));

            localStorage.setItem('favorites', JSON.stringify(newFavorites));
        }
    };
    const handleFilled = (favoritesList) => favoritesList?.map(item => item.id);
    console.log(favorites);
    return (
        <React.Fragment>
            { show && (
                <Modal
                    handleModalClose={handleModalClose}
                    addToCart={addToCart}
                />
            )}
            <Cards
                cards={favorites}
                filled={filled}
                onClick={handleModalOpen}
                updateFavorite={deleteFavorite}
            />
        </React.Fragment>
    )
}
export default Favorites;
