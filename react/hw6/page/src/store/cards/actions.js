import types from './types'

const setCardsData = (cardsData) => ({ type: types.SET_CARDS_DATA, data: cardsData });
export default {
  setCardsData,

}