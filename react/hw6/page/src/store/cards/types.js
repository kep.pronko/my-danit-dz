const SET_CARDS_DATA = 'app/cards/SET_CARDS_DATA';

export default {
  SET_CARDS_DATA
}