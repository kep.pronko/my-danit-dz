import axios from "axios";
import actions from './actions'



export default {
  setFavorites: actions.setFavorites,
  setFilled: actions.setFilled
}
