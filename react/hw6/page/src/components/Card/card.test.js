import { render } from "@testing-library/react";
import { Card } from "./Card";
import userEvent from "@testing-library/user-event";
import jest from 'jest-mock';

describe("Card tests", () => {
    test("Smoke test card", () => {
        render(<Card item={{ name: 'naem', price: 1230, url: '1241', articul: 123, id: 1 }} />);
    });
    test("Add to cart card", () => {
        const onClick = jest.fn();

        const { getByTestId } = render(<Card item={{ name: 'naem', price: 1230, url: '1241', articul: 123, id: 1 }} onClick={onClick} />);

        const addToCartButton = getByTestId("add-to-cart");
        expect(onClick).toHaveBeenCalledTimes(0);
        userEvent.click(addToCartButton);
        expect(onClick).toHaveBeenCalledTimes(1);
    });
    test("Delete from cart", () => {
        const cart = [1];
        const deleteCart = jest.fn((item) => {
            cart.splice(cart.indexOf(item.id), 1);
        });
        const { getByTestId } = render(<Card item={{ name: 'naem', price: 1230, url: '1241', articul: 123, id: 1 }} deleteCart={deleteCart} close={true} />);

        const Delete = getByTestId("delete");
        expect(deleteCart).toHaveBeenCalledTimes(0);
        userEvent.click(Delete);
        expect(deleteCart).toHaveBeenCalledTimes(1);
        expect(cart).not.toContain(1);
    });
    test("Add to favourite", () => {
        const filled = [];
        const updateFavorite = jest.fn((item) => {
            filled.push(item.id);
        });
        const { getByTestId } = render(<Card item={{ name: 'naem', price: 1230, url: '1241', articul: 123, id: 1 }} updateFavorite={updateFavorite} />);

        const addToFavourite = getByTestId("icon");
        expect(updateFavorite).toHaveBeenCalledTimes(0);
        userEvent.click(addToFavourite);
        expect(updateFavorite).toHaveBeenCalledTimes(1);
        expect(filled).toContain(1);
    });

    test("Delete from favourite ", () => {
        const filled = [1];
        const updateFavorite = jest.fn((item) => {

            filled.splice(filled.indexOf(item.id), 1);
        });
        const { getByTestId } = render(<Card item={{ name: 'naem', price: 1230, url: '1241', articul: 123, id: 1 }} updateFavorite={updateFavorite} />);


        const Delete = getByTestId("icon");
        expect(updateFavorite).toHaveBeenCalledTimes(0);
        userEvent.click(Delete);
        expect(updateFavorite).toHaveBeenCalledTimes(1);
        expect(filled).not.toContain(1);
    });
});
