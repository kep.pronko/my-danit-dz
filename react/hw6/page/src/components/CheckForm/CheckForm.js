import React from "react";
import { Form, withFormik } from "formik";
import CheckInput from "./CheckInput";
import * as yup from "yup";
import "./Check.scss";
import { cartOperations } from "../../store/cart";
import store from "../../store";

const validation = yup.object().shape({
  name: yup.string().min(2, "Короткое имя").required("Обязательное поле"),
  surname: yup.string().min(2, "Короткая фамилия").required("Обязательное поле"),
  age: yup.number().max(110, "Не корректный возраст").required("Обязательное поле"),
  adress: yup.string().required("Обязательное поле"),
  phone: yup.string().required("Обязательное поле"),
});

const CheckoutForm = (props) => {
  return (
    <Form className="checkForm">
      <h2>Данные для формления заказа</h2>
      <CheckInput name="name" placeholder="Имя" type="text" />
      <CheckInput name="surname" placeholder="Фамилия" type="text" />
      <CheckInput name="age" placeholder="Возраст" type="number" />
      <CheckInput name="adress" placeholder="Адрес" type="text" />
      <CheckInput name="phone" placeholder="Номер телефона" type="number" />
      <div>
        <button type="submit" disabled={props.isSubmitting}>
          Купить
        </button>
      </div>
    </Form>
  );
};

export default withFormik({
  mapPropsToValues: () => ({
    name: "",
    surname: "",
    age: "",
    adress: "",
    phone: "",
  }),
  validationSchema: validation,
  handleSubmit: (values, helpers) => {
    console.log(`Ваш заказик ${localStorage.getItem("cart")}`);
    localStorage.removeItem("cart");
    store.dispatch(cartOperations.deleteCart());
    console.log(values);
    helpers.setSubmitting(false);
    helpers.resetForm();
  },
})(CheckoutForm);
