import React from "react";
import { useField } from "formik";
import "./Check.scss";

const CheckInput = ({ name, ...rest }) => {
  const [field, fieldState] = useField(name);

  return (
    <div>
      <input {...field} {...rest} />
      {fieldState.error && fieldState.touched && (
        <p className="errors">{fieldState.error}</p>
      )}
    </div>
  );
};

export default CheckInput;
