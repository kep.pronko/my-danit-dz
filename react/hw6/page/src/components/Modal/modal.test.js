import { Modal } from "./Modal";
import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import jest from 'jest-mock';

describe("Modal tests", () => {
    test("Smoke modal test", () => {
        render(<Modal modals={{ header: 'Реально добавить это платье в корзину?', textModal: 'ble' }} />);
    });
    test("Close modal test", () => {
        const handleModalClose = jest.fn();
        const addToCart = jest.fn();

        const { getByTestId } = render(
            <Modal
                modals={{ header: 'Реально добавить это платье в корзину?', textModal: 'ble' }}
                addToCart={addToCart}
                handleModalClose={handleModalClose}
            />
        );

        const cancelButton = getByTestId("modal-cancels");
        expect(handleModalClose).toHaveBeenCalledTimes(0);
        userEvent.click(cancelButton);
        expect(handleModalClose).toHaveBeenCalledTimes(1);

        const cancelCrossButton = getByTestId("modal-cancel-cross");
        expect(handleModalClose).toHaveBeenCalledTimes(1);
        userEvent.click(cancelCrossButton);
        expect(handleModalClose).toHaveBeenCalledTimes(2);
    });
    test("Add to cart test", () => {
        const handleModalClose = jest.fn();
        const addToCart = jest.fn();

        const { getByTestId } = render(
            <Modal
                modals={{ header: 'Реально добавить это платье в корзину?', textModal: 'ble' }}
                addToCart={addToCart}
                handleModalClose={handleModalClose}
            />
        );

        const addToCartButton = getByTestId("modal-success-button");
        expect(addToCart).toHaveBeenCalledTimes(0);
        expect(handleModalClose).toHaveBeenCalledTimes(0);
        userEvent.click(addToCartButton);
        expect(addToCart).toHaveBeenCalledTimes(1);
        expect(handleModalClose).toHaveBeenCalledTimes(0);
    });

});
