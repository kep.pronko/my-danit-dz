import React from "react";
import "./modal.scss";
import { cartSelectors } from '../../store/cart';
import { connect } from "react-redux";

export const Modal = ({ closeButton, handleModalClose, addToCart, modals }) => {


  return (
    <div className="modal-background" onClick={handleModalClose}>
      <div className="modal-card" onClick={(e) => e.stopPropagation()}>
        <div className="modal-header">
          <h2 className="modal-title">{modals.header}</h2>
          <div className="modal-close" hidden={!closeButton}>
            <button className="btn"
              data-testid="modal-cancel-cross" onClick={handleModalClose}>
              X
              </button>
          </div>
        </div>
        <div className="modal-body">
          <p className="modal-text">{modals.textModal}</p>
          <div className="modal-btn">
            <button className="modal-success"
              data-testid="modal-success-button" onClick={addToCart}>
              Ok
              </button>
            <button className="modal-cancel"
              data-testid="modal-cancels" onClick={handleModalClose}>
              Cancel
              </button>
          </div>
        </div>
      </div>
    </div>
  );

}
const mapStateToProps = (state) => ({
  modals: cartSelectors.getModal()(state)
});
export default connect(mapStateToProps)(Modal);
