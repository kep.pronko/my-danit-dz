import React from 'react';

const Page404 = (props) => (
  <h2>404 - Page not found</h2>
);

export default Page404;