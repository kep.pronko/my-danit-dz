import React, { useEffect } from "react";
import "./App.scss";
import AppRoutes from "./routes/AppRoutes";
import Menu from './components/Menu/Menu';
import { useDispatch } from "react-redux";
import { cardsOperations } from './store/cards';



const App = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(cardsOperations.getCard())
  }, [dispatch]);
  return (
    <React.Fragment>
      <Menu />
      <AppRoutes />
    </React.Fragment>
  )
}

export default App;


