import React from 'react';
import { Route, Switch, Redirect } from "react-router-dom";
import Favorites from "../components/Favorites/Favorites";
import Cart from '../components/Cart/Cart';
import Main from '../components/Main/Main';
import Page404 from "../components/Page404/Page404";

const AppRoutes = () => {
  return (
    <Switch>
      <Redirect exact from='/' to='/main' />
      <Route exact path='/main'><Main /></Route>
      <Route exact path='/favorites'><Favorites /></Route>
      <Route exact path='/cart'><Cart /></Route>
      <Route path='*'><Page404 /></Route>
    </Switch>
  )
};

export default AppRoutes;