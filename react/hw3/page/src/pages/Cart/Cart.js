import React, { useState, useEffect } from "react";
import Modal from "../../components/Modal/Modal";
import Cards from "../../components/Cards/Cards";

const modals = [
    {
        id: 1,
        header: "Реально добавить это платье в корзину?",
        closeButton: true,
        text: "Бомба",
    }
];
const Cart = () => {
    const [show, setShow] = useState(false);
    const [modal, setModal] = useState(null);
    const [cart, setCart] = useState([]);
    const [chouseId, setChouseId] = useState(null);
    // TODO: need improve but right now is ok

    useEffect(() => {
        const localStorageCart = JSON.parse(localStorage.getItem('cart'));
        setCart(localStorageCart);
    }, []);

    const handleModalClose = () => {
        setShow(false);
        setChouseId(null);
    };

    const handleModalOpen = (event) => {
        const index = Number(event.target.id);
        const modal = modals.find((v) => v.id === 1);
        setModal(modal);
        setShow(true);
        setChouseId(index);
    };

    const addToCart = () => {
        const index = cart.concat(chouseId)
        setCart(index);
        setShow(false);
        window.localStorage.setItem('cart', index);
    };
    const deleteCart = (favoriteCart) => {
        let newCart = [...cart];
        const carts = [];
        carts.push(newCart)

        const index = newCart.findIndex(item => item.id === favoriteCart.id);
        if (index > -1) {
            newCart.splice(index, 1);
            setCart(newCart);

            localStorage.setItem('cart', JSON.stringify(newCart));
        }
    };


    return (
        <React.Fragment>
            { show && (
                <Modal
                    header={modal.header}
                    closeButton={modal.closeButton}
                    textModal={modal.text}
                    handleModalClose={handleModalClose}
                    addToCart={addToCart}
                />
            )}
            <Cards
                close={true}
                cards={cart}
                filled={[]}
                deleteCart={deleteCart}
                onClick={handleModalOpen}
            />
        </React.Fragment>
    )
}

export default Cart
