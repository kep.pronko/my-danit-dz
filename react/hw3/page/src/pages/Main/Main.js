import React, { useState, useEffect } from "react";
import Modal from "../../components/Modal/Modal";
import Cards from "../../components/Cards/Cards";

const modals = [
    {
        id: 1,
        header: "Реально добавить это платье в корзину?",
        closeButton: true,
        text: "Бомба",
    },
    {
        id: 2,
        header: "Do you want to add this file?",
        closeButton: false,
        text: "Bla Bla Bla",
    },
];

const Main = ({ cards }) => {
    const [show, setShow] = useState(false);
    const [modal, setModal] = useState(null);
    const [favorites, setFavorites] = useState([]);
    const [cart, setCart] = useState([]);
    const [chouseId, setChouseId] = useState(null);

    const [filled, setFilled] = useState([]);

    useEffect(() => {
        const localStorageFavorites = JSON.parse(localStorage.getItem('favorites'));
        setFavorites(localStorageFavorites);
        setFilled(handleFilled(localStorageFavorites));

        const localStorageCart = JSON.parse(localStorage.getItem('cart'));
        setCart(localStorageCart);
    }, []);

    const handleModalClose = () => {
        setShow(false);
        setChouseId(null);
    };

    const handleModalOpen = (event) => {
        const index = Number(event.target.id);
        const modal = modals.find((v) => v.id === 1);
        setModal(modal);
        setShow(true);
        setChouseId(index);
    };

    const addToCart = () => {
        const newCart = cart ? [...cart] : [];
        const card = cards?.find(card => card.id === chouseId);
        newCart.push(card);
        setCart(newCart);
        setShow(false);

        window.localStorage.setItem('cart', JSON.stringify(newCart));
    };

    const updateFavorite = (favoriteItem) => {
        let newFavorites = favorites ? [...favorites] : [];
        const favorite = [];
        favorite.push(favoriteItem)

        const itemAlreadyExist = favorites?.find(item => item.id === favoriteItem.id);

        if (itemAlreadyExist) {
            const index = newFavorites?.findIndex(item => item.id === favoriteItem.id);
            newFavorites.splice(index, 1);
        } else {
            newFavorites = [...newFavorites, ...favorite];
        }

        setFilled(handleFilled(newFavorites));

        setFavorites(newFavorites);
        localStorage.setItem('favorites', JSON.stringify(newFavorites));
    };

    const handleFilled = (favoritesList) => favoritesList?.map(item => item.id);

    return (
        <React.Fragment>
            { show && (
                <Modal

                    header={modal.header}
                    closeButton={modal.closeButton}
                    textModal={modal.text}
                    handleModalClose={handleModalClose}
                    addToCart={addToCart}
                />
            )}
            <Cards filled={filled} cards={cards} onClick={handleModalOpen} updateFavorite={updateFavorite} />
        </React.Fragment>
    )
}

export default Main

