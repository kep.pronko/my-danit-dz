import React, { useState, useEffect } from "react";
import Modal from "../../components/Modal/Modal";
import Cards from "../../components/Cards/Cards";

const modals = [
    {
        id: 1,
        header: "Реально добавить это платье в корзину?",
        closeButton: true,
        text: "Бомба",
    }
];
const Favorites = () => {
    const [show, setShow] = useState(false);
    const [modal, setModal] = useState(null);
    const [favorites, setFavorites] = useState([]);
    const [cart, setCart] = useState([]);
    const [chouseId, setChouseId] = useState(null);

    const [filled, setFilled] = useState([]);

    useEffect(() => {
        const localStorageFavorites = JSON.parse(localStorage.getItem('favorites'));
        setFavorites(localStorageFavorites);
        setFilled(handleFilled(localStorageFavorites));
    }, []);

    const handleModalClose = () => {
        setShow(false);
        setChouseId(null);
    };

    const handleModalOpen = (event) => {
        const index = Number(event.target.id);
        const modal = modals.find((v) => v.id === 1);
        setModal(modal);
        setShow(true);
        setChouseId(index);
    };

    const addToCart = () => {
        const index = cart.concat(chouseId)
        setCart(index);
        setShow(false);
        window.localStorage.setItem('cart', index);
    };

    const deleteFavorite = (favoriteItem) => {
        let newFavorites = [...favorites];
        const favorite = [];
        favorite.push(favoriteItem)

        const index = newFavorites.findIndex(item => item.id === favoriteItem.id);
        if (index !== -1) {
            newFavorites.splice(index, 1);
            setFavorites(newFavorites);

            localStorage.setItem('favorites', JSON.stringify(newFavorites));
        }
    };

    const handleFilled = (favoritesList) => favoritesList?.map(item => item.id);
    console.log(favorites);
    return (
        <React.Fragment>
            { show && (
                <Modal
                    header={modal.header}
                    closeButton={modal.closeButton}
                    textModal={modal.text}
                    handleModalClose={handleModalClose}
                    addToCart={addToCart}
                />
            )}
            <Cards
                cards={favorites}
                filled={filled}
                onClick={handleModalOpen}
                updateFavorite={deleteFavorite}
            />
        </React.Fragment>
    )
}
export default Favorites;
