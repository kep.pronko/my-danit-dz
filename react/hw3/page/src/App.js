import React, { useState, useEffect } from "react";
import "./App.scss";
import axios from 'axios';
import AppRoutes from "./routes/AppRoutes";
import Menu from './components/Menu/Menu';


const App = () => {
  const [cards, setCards] = useState([]);

  useEffect(() => {
    axios("/api/card").then((res) => {
      setCards(res.data);
    });
  }, []);
  return (
    <React.Fragment>
      <Menu />
      <AppRoutes cards={cards} />
    </React.Fragment>
  )
}

export default App;
