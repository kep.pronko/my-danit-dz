import React from "react";
import "./Button.scss";

class Button extends React.Component {
  render() {
    const { backgroundColor, buttonText, onClick, id } = this.props;
    return (
      <div>
        <button
          id={id}
          className="button"
          onClick={onClick}
          style={{ backgroundColor: backgroundColor }}
        >
          {buttonText}
        </button>
      </div>
    );
  }
}
export default Button;
