import React, { Component } from "react";
import "./modal.scss";

export default class Modal extends Component {
  render() {
    const { header, closeButton, textModal, handleModalClose, addToCart } = this.props;
    return (
      <div className="modal-background" onClick={handleModalClose}>
        <div className="modal-card" onClick={(e) => e.stopPropagation()}>
          <div className="modal-header">
            <h2 className="modal-title">{header}</h2>
            <div className="modal-close" hidden={!closeButton}>
              <button className="btn" onClick={handleModalClose}>
                X
              </button>
            </div>
          </div>
          <div className="modal-body">
            <p className="modal-text">{textModal}</p>
            <div className="modal-btn">
              <button className="modal-success" onClick={addToCart}>
                Ok
              </button>
              <button className="modal-cancel" onClick={handleModalClose}>
                Cancel
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
