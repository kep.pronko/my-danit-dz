import React, {Component} from 'react';
import Error from "../../pages/Error/Error";

class ErrorBoundary extends Component {
  state = {
    errorPresent: false
  }

  // componentDidCatch(error, errorInfo) {
  //   this.setState({errorPresent: true})
  //   axios.post('/error', {error, errorInfo})
  // }

  static getDerivedStateFromError(error) {
    return {errorPresent: true}
  }

  render() {
    const {children} = this.props;
    const {errorPresent} = this.state;

    return errorPresent ? <Error /> : children
  }
}

export default ErrorBoundary;