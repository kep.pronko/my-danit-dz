import React from 'react'
import { Link } from 'react-router-dom';
import './menu.scss';

const NAVIGATION = [
    { name: 'Main', path: '/main' },
    { name: 'Favorites', path: '/favorites' },
    { name: 'Cart', path: '/cart' },
]

const Menu = () => {
    return (
        <div className="cr-navbar">
            {NAVIGATION.map((item, index) => {
                return (
                    <Link className="cr-navbar__link" key={index} to={item.path}>{item.name}</Link>
                )
            }
            )}
        </div>
    )
}

export default Menu
