import React from 'react'
import { Card } from '../Card/Card'
import './Cards.scss'

const Cards = ({ onClick, deleteCart, updateFavorite, filled, cards, close }) => {
  return (
    <div className="wrapper-card">
      {cards.map(item => (
        <div className="products-page-products-wrapper__item" key={item.id} id={item.id}>
          <Card close={close} filled={filled} item={item} id={item.id} onClick={onClick} deleteCart={deleteCart} updateFavorite={updateFavorite} />
        </div>))
      }
    </div>
  )
};

export default Cards;
