import React, { useEffect } from "react";
import { Modal } from "../../components/Modal/Modal";
import Cards from "../../components/Cards/Cards";
import { cartOperations } from '../../store/cart';
import { cartSelectors } from '../../store/cart';
import { useDispatch, useSelector } from "react-redux";
import CheckoutForm from "../../components/CheckForm/CheckForm";


const Cart = () => {
    const show = useSelector(cartSelectors.getShow());
    const cart = useSelector(cartSelectors.getCart());
    const chouseId = useSelector(cartSelectors.getChouseId());
    const dispatch = useDispatch();

    useEffect(() => {
        const localStorageCart = JSON.parse(localStorage.getItem('cart'));
        dispatch(cartOperations.setCartData(localStorageCart));
    }, [dispatch]);

    const handleModalClose = () => {
        dispatch(cartOperations.setShowModal(false));
        dispatch(cartOperations.setChouseId(null));
    };

    const handleModalOpen = (event) => {
        const index = Number(event.target.id);
        dispatch(cartOperations.setShowModal(true));
        dispatch(cartOperations.setChouseId(index));
    };

    const addToCart = () => {
        const index = cart.concat(chouseId)
        dispatch(cartOperations.setCartData(index));
        dispatch(cartOperations.setShowModal(false));
        window.localStorage.setItem('cart', index);
    };
    const deleteCart = (favoriteCart) => {
        let newCart = [...cart];
        const carts = [];
        carts.push(newCart)

        const index = newCart.findIndex(item => item.id === favoriteCart.id);
        if (index > -1) {
            newCart.splice(index, 1);
            dispatch(cartOperations.setCartData(newCart));

            localStorage.setItem('cart', JSON.stringify(newCart));
        }
    };
    const check = cart?.length > 0;

    return (
        <React.Fragment>
            { show && (
                <Modal
                    handleModalClose={handleModalClose}
                    addToCart={addToCart}
                />
            )}
            <Cards
                close={true}
                cards={cart}
                filled={[]}
                deleteCart={deleteCart}
                onClick={handleModalOpen}
            />
            {check && <CheckoutForm />}
        </React.Fragment>
    )
}

export default Cart
