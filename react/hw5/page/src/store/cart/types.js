const SET_SHOW_MODAL = 'app/cart/SET_SHOW_MODAL';
const SET_MODAL_DATA = 'app/cart/SET_MODAL_DATA';
const SET_CART_DATA = 'app/cart/SET_CART_DATA';
const SET_CHOUSE_ID = 'app/cart/SET_CHOUSE_ID';
const DELETE_CART = 'app/cards/DELETE_CART';

export default {
  SET_SHOW_MODAL,
  SET_MODAL_DATA,
  SET_CART_DATA,
  SET_CHOUSE_ID,
  DELETE_CART
}