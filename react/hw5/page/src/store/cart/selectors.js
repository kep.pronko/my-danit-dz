
const getShow = () => state => state.cart.show;
const getModal = () => state => state.cart.modal;
const getCart = () => state => state.cart.cart;
const getChouseId = () => state => state.cart.chouseId;
export default {
  getShow,
  getModal,
  getCart,
  getChouseId
}