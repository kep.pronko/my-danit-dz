
import actions from './actions'



export default {
  setShowModal: actions.setShowModal,
  setCartData: actions.setCartData,
  setModalData: actions.setModalData,
  setChouseId: actions.setChouseId,
  deleteCart: actions.deleteCart,
}
