import reducer from "./reducers";

export {default as favoriteOperations} from './operations'
export {default as favoriteSelectors} from './selectors'

export default reducer;
