import React from 'react'
import './Card.scss'
import Icon from "../Icon/Icon";
import { useSelector } from "react-redux";
import { favoriteSelectors } from '../../store/favorite';

export const Card = ({ item, onClick, updateFavorite, deleteCart, close }) => {
    const { name, price, url, articul, id } = item;
    const filled = useSelector(favoriteSelectors.getFilled());
    const filledIndex = filled?.indexOf(id);

    return (
        <div className="product-item">
            <div className="product-list">
                {!close && <div className="icon" onClick={() => updateFavorite(item)}>
                    <Icon filled={filledIndex > -1 ? true : false} id={id} type='star' />
                </div>}
                {close && <div onClick={() => deleteCart(item)}>X</div>}


                <img height='250px' alt='dress' src={url} />

                <h3>{name}</h3>
                <span className="price">{price}$</span>
                <span className="price">Артикуль: {articul}</span>
                <button id={id} onClick={onClick} className="button">В корзину</button>
            </div>
        </div>

    )
}
