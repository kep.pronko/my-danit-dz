import React from "react";
import * as Icons from '../../theme/icon'
import './Icon.scss'

const Icon = ({type, color,id,addToFavorite,filled}) => {
  const jsx = Icons[type];
  if (!jsx) {
    return null;
  }
  return (
    <div id={id} onClick={addToFavorite} className={`icon icon--${type}`}>
      {jsx({color, filled,id})}
    </div>
  )
};

export default Icon;