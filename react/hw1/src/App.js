import React from "react";
import "./App.css";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

const modals = [
  {
    id: 1,
    header: "Do you want to delete this file?",
    closeButton: true,
    text: "Once you delete this file, it won’t be possible to undo this ac",
  },
  {
    id: 2,
    header: "Do you want to add this file?",
    closeButton: false,
    text: "Bla Bla Bla",
  },
];
const buttons = [
  {
    id: 1,
    backgroundColors: "#8CC152",
    text: "Open first modal",
  },
  {
    id: 2,
    backgroundColor: "#E9573F",
    text: "Open second modal",
  },
];
class App extends React.Component {
  state = {
    show: false,
    modal: {},
  };
  handleModalClose = () => {
    this.setState({ show: false });
  };

  handleModalOpen = (event) => {
    const index = Number(event.target.id);
    const modal = modals.find((v) => v.id === index);
    this.setState({ modal: modal, show: true });
  };
  render() {
    const { modal } = this.state;
    const button1 = buttons.find((v) => v.id === 1);
    const button2 = buttons.find((v) => v.id === 2);

    if (!modal) {
      return <div>isLoading</div>;
    }

    return (
      <>
        <Button
          backgroundColor={button1.backgroundColors}
          buttonText={button1.text}
          onClick={this.handleModalOpen}
          id={1}
        ></Button>
        <Button
          backgroundColor={button2.backgroundColor}
          buttonText={button2.text}
          onClick={this.handleModalOpen}
          id={2}
        ></Button>
        {this.state.show && (
          <Modal
            header={modal.header}
            closeButton={modal.closeButton}
            textModal={modal.text}
            handleModalClose={this.handleModalClose}
          />
        )}
      </>
    );
  }
}

export default App;
