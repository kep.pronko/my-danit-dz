import React from "react";
import "./App.scss";
import Modal from "./components/Modal/Modal";
import  Cards  from "./components/Cards/Cards";
import axios from 'axios';


const modals = [
  {
    id: 1,
    header: "Реально добавить это платье в корзину?",
    closeButton: true,
    text: "Бомба",
  },
  {
    id: 2,
    header: "Do you want to add this file?",
    closeButton: false,
    text: "Bla Bla Bla",
  },
];
class App extends React.Component {
  state = {
    show: false,
    modal: {},
    cards: [],
    favorites: [],
    cart: [],
    chouseId: null,
    filled: false,
  };
   componentDidMount=() =>{
   axios('/api/card.js').then(res => { console.log(res); this.setState({cards: res.data});})
   

  }
  handleModalClose = () => {
    this.setState({ show: false,chouseId: null });
  };

  handleModalOpen = (event) => {
    const index = Number(event.target.id);
    const modal = modals.find((v) => v.id === 1);
    this.setState({ modal: modal, show: true, chouseId:index });
  };
  addToCart = () => {
    const index =this.state.cart.concat(this.state.chouseId)
    this.setState({ cart: index, show: false });
    window.localStorage.setItem('cart', index);
  };
  addToFavorite = (event) => {
    const id = Number(event.target.id);
    const favorite =this.state.favorites;
    if (!favorite.includes(id)){
      const favorites =this.state.favorites.concat(id)
    this.setState({ favorites: favorites});
    window.localStorage.setItem('favorites', favorite);
    }
    if(favorite.includes(id)){
     const index = favorite.indexOf(id);
     console.log(index);
     const favorites = favorite.splice(index, 1);
     this.setState({ favorites: favorites});
     window.localStorage.setItem('favorites', favorite);
    }
    
  };
  render() {
    const { modal ,cards,filled,favorites} = this.state;
  

    if (!modal) {
      return <div>isLoading</div>;
    }
    if (cards.length === 0) {
      return <div>isLoading</div>;
    }
    return (
      <React.Fragment>
       
        {this.state.show && (
          <Modal
            header={modal.header}
            closeButton={modal.closeButton}
            textModal={modal.text}
            handleModalClose={this.handleModalClose}
            addToCart ={this.addToCart}
          />
        )}
        <Cards favorites={favorites} filled={filled} cards ={cards}  onClick={this.handleModalOpen} addToFavorite={this.addToFavorite}/>
      </React.Fragment>
    );
  }
}

export default App;
