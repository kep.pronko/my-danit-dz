import React,{useState,useEffect} from 'react'
import {Card} from '../Card/Card'
import './Cards.scss'
import axios from 'axios'


    const Cards = ({onClick,addToFavorite,filled,favorites}) => {
        const [email, setEmail] = useState([]);
       
        useEffect(() => {
          axios(`/api/card.js`)
            .then(res => {
              setEmail(res.data);
            })
        }, [])
        
      
      
        return (
          <div className="wrapper-card">
            {email
          .map(item => (<div className="products-page-products-wrapper__item" key={item.id} id={item.id}>
          <Card favorites={favorites} filled={filled} item={item} id ={item.id} onClick={onClick} addToFavorite={addToFavorite}/>
        </div>))}
          </div>
        )
      };
      export default Cards;
 