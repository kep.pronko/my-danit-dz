import React from 'react'
import './Card.scss'
import Icon from "../Icon/Icon";

export const Card = ({item,onClick,addToFavorite,favorites}) => {
    const {name,price,url,articul,id} = item;
    const check = favorites.includes(id);
    
    return (
       <div className="product-item">
        <div className="product-list">
            <div className="icon">
                {check && <Icon  filled={true} addToFavorite={addToFavorite} id={id} type='star'/>}
                {!check && <Icon  filled={false} addToFavorite={addToFavorite} id={id} type='star'/>}
            
            </div>

        <img height='250px' src={url}/>
        
          <h3>{name}</h3>
            <span className="price">{price}$</span>
            <span className="price">Артикуль: {articul}</span>
            <button id={id} onClick={onClick} className="button">В корзину</button>
        </div>
        </div>
     
    )
}
