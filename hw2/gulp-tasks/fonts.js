const { src, dest } = require("gulp");

const fonts = () => {
   return src("./src/fonts/*")
   .pipe( dest("./dist/fonts")
      
   );
};

exports.fonts = fonts;