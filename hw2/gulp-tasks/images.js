const { src, dest } = require("gulp");
const imageMin = require("gulp-imagemin");

const images = () => {
   return src("./src/images/*.{png,jpg,jpeg,gif,svg}")
   .pipe(imageMin({
      progressive: true,
      interlaced: true,
      optimizationLevel: 5,
   }))
   .pipe( dest("./dist/images")
      
   );
};

exports.images = images;
