const { src, dest } = require("gulp");
const sass = require("gulp-sass");
const concat = require("gulp-concat");
const cleanCss = require("gulp-clean-css");
const autoprefixer = require("gulp-autoprefixer");
const mediaQueryOptimizer = require("gulp-group-css-media-queries");
const browserSync = require("browser-sync");

const style = () => {
   return src("./src/styles/*.scss")
      .pipe(concat("styles.min.css"))
      .pipe(sass().on("error", sass.logError))
      .pipe(autoprefixer({
         overrideBrowserList: ["last 2 versions"],
         cascade: true
      }))
      .pipe(mediaQueryOptimizer())
      .pipe(cleanCss())
      .pipe(dest("./dist/css"))
      .pipe(browserSync.stream());
};

exports.style = style;

