const { series, parallel } = require("gulp");
const { style } = require("./gulp-tasks/style.js");
const { serv } = require("./gulp-tasks/serv.js");
const { scripts } = require("./gulp-tasks/script.js");
const { images } = require("./gulp-tasks/images.js");
const { watch } = require("./gulp-tasks/watch");
const { fonts } = require("./gulp-tasks/fonts.js");


const build = series( style, scripts, images,fonts);
const dev = parallel(serv, watch, series(style, scripts, images,fonts));

exports.default = dev;
exports.dev = dev;
exports.build = build;
