
const root = document.getElementById("root");
class Planet {
   constructor() {
   }
   request() {
      return axios.get("https://ajax.test-danit.com/api/swapi/films");
   }
   getInfo() {
      this.request().then(
         function ({ data }) {
            data.forEach(el => {
               const div = document.createElement("div");
               document.getElementById("root").append(div);
               const hr = document.createElement("hr");
               div.append(hr);
               const pEpisod = document.createElement("p");
               pEpisod.textContent = el.episodeId;
               div.append(pEpisod);
               const pName = document.createElement("p");
               pName.textContent = el.name;
               div.append(pName);
               const pText = document.createElement("p");
               pText.textContent = el.openingCrawl;
               div.append(pText);
               el.characters.forEach(element => {
                  axios.get(element).then(
                  function ({ data }) {
                     div.append(this.render(data));
                  }.bind(this)
               );
               })
            })
            console.log(data);
         }.bind(this)
      );
   }
   render(object) {
      const fragment = document.createDocumentFragment();
      for (const key in object) {
         const p = document.createElement("p");
         p.textContent = object[key];
         fragment.append(p);
      }
      return fragment;
   }
}

const pla = new Planet();
pla.getInfo();