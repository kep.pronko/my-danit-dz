class Employee {
    constructor(name,age,salary){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }
    get age() {
        return this._age;
    }
    set age(value) {
        this._age = value;
    }get salary() {
        return this._salary;
    }
    set salary(value) {
        this._salary = value;
    }
}
class Programmer extends Employee{
    constructor(name,age,salary,lang){
        super(name,age,salary)
        this.lang = lang;
    }
    get salary() {
        return this._salary ;
    }
    set salary(value) {
        return (this._salary = value * 3 );
    }
}
const programmer1 = new Programmer("Dan", 18,2000,"piton");
console.log(programmer1);
const programmer2 = new Programmer("Ben", 22,3333,"js");
console.log(programmer2);
const programmer3 = new Programmer("Ken", 33,4444,"java");
console.log(programmer3);