const root = document.getElementById("root");
const button = document.getElementById("btn");
button.addEventListener("click", get);
async function get (){
  const ip = await axios.get("https://api.ipify.org/?format=json").then(
    function ({ data }) {
      return data.ip;
    }
 ); 
 const region = await axios.get('http://ip-api.com/xml/' + ip).then(
  function ({data}) {
    const p = document.createElement("p");
    p.textContent = data;
    root.append(p);
  }
);
}